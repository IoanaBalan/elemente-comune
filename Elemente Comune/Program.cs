﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elemente_Comune
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] v1 = { 2, 4, 67, 1, 3, 56 };
            int[] v2 = { 4, 76, 34, 12, 1, 44, 3, 23 };

            for(int i=0; i<v1.Length; i++)
                for(int j=0; j<v2.Length; j++)
                    if(v1[i] == v2[j])
                    {
                        Console.Write(v1[i] + " ");
                        break;
                    }
            Console.ReadKey();
        }
    }
}
